module bitbucket.org/buuuksg/ghost

go 1.12

require (
	github.com/aws/aws-lambda-go v1.13.3
	github.com/aws/aws-sdk-go v1.27.2
	github.com/caarlos0/env/v5 v5.1.4
	github.com/casbin/casbin v1.9.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fortytw2/dockertest v0.0.0-20181228171220-480d52efdffe
	github.com/go-playground/validator/v10 v10.1.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/imdatngo/gowhere v1.1.1
	github.com/imdatngo/mergo v0.3.9
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.1.13
	github.com/labstack/gommon v0.3.0
	github.com/segmentio/ksuid v1.0.2
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gormigrate.v1 v1.6.0
)
