# Buuuk Guidelines

> A curated list of high quality coding style conventions and standards.

A set of guidelines for a specific programming language that recommends programming style, common patterns and best practices should be followed.

## Contents
1. [Programming Languages](#programming-languages)
    1. [Golang](#golang)
       * [Time Format](#time-format)
    2. [JSON](#json)
       * [Starting Documents](#starting-documents)
       * [Property Name Format](#property-name-format)
       * [Singular vs Plural Property Names](#singular-vs-plural-property-names)
       * [Latitude/Longitude Properties](#latitude/longitude-properties)
2. [Development Environment](#development-environment)

## Programming Languages

### Golang

#### Time Format
RFC3339 format is applied for all time objects.

### JSON

#### Starting Documents
All JSON documents must be started by a JSON Object.\
Good:
```json
{
  "data": [
    {
      "address": "address 1"
    },
    {
      "address": "address 2"
    }
  ]
}
```
Bad:
```json
[
  {
    "address": "address 1"
  },
  {
    "address": "address 2"
  }
]
```

#### Property Name Format
Property names must conform to the following guidelines:
- Property names should be meaningful names with defined semantics.
- Property names must be snake_cased, ascii strings.
- The first character must be a letter, an underscore (_) or a dollar sign ($).
- Subsequent characters can be a letter, a digit, an underscore, or a dollar sign.
- Reserved Javascript keywords should be avoided (<b>class</b>, <b>function</b>, etc.).

#### Singular vs Plural Property Names
Array types should have plural property names. All other property names should be singular.
```json
{
  "author": "lisa",
  "articles": [
    {
      "title": "Title 1"
    },
    {
      "title": "Title 2"
    },
  ]
}
```

#### Latitude/Longitude Properties
Latitude and Longitude property names should be named as <b>lat</b> and <b>lng</b>. The datatype of both should be <b>float64.</b>\
Good:
```json
{
  "lat": 1.123,
  "lng": 102.1213
}
```
Bad:
```json
{
  // Wrong datatype for lat
  "lat": "1.123",
  // Wrong naming convention
  "lon": 102.1213
}
```

## Development Environment
