package main

import (
	"bitbucket.org/buuuksg/ghost/internal/functions/migration"
	"fmt"
)

func main() {
	if err := migration.Run(); err != nil {
		fmt.Printf("ERROR: %+v\n", err)
	}
}
