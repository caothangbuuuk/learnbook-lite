// GHOST - Golang API Starter Kits
//
// API documents for GHOST.
//
// ## Authentication
// All API endpoints started with version, ex: `/v1/*`, require authentication token.
// Firstly, grab the **access_token** from the response of `/login`. Then include this header in all API calls:
// ```
// Authorization: Bearer ${access_token}
// ```
//
// For testing directly on this Swagger page, use the `Authorize` button right here bellow.
//
// Terms Of Service: N/A
//
//     Host: %{HOST}
//     Version: 1.5.0
//     Contact: Dat Ngo <dat@buuuk.com>
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - login: []
//     - bearer: []
//
//     SecurityDefinitions:
//     login:
//         type: oauth2
//         tokenUrl: /login
//         refreshUrl: /refresh
//         flow: password
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package main

import (
	"bitbucket.org/buuuksg/ghost/config"
	"bitbucket.org/buuuksg/ghost/internal/api/auth"
	"bitbucket.org/buuuksg/ghost/internal/api/comment"
	"bitbucket.org/buuuksg/ghost/internal/api/feed"
	"bitbucket.org/buuuksg/ghost/internal/api/like"
	"bitbucket.org/buuuksg/ghost/internal/api/share"
	"bitbucket.org/buuuksg/ghost/internal/api/user"
	"bitbucket.org/buuuksg/ghost/internal/api/userhiddenfeed"
	"bitbucket.org/buuuksg/ghost/internal/api/userrelationship"
	"bitbucket.org/buuuksg/ghost/internal/api/usertaggedfeed"
	"bitbucket.org/buuuksg/ghost/internal/rbac"
	dbutil "bitbucket.org/buuuksg/ghost/internal/util/db"
	_ "bitbucket.org/buuuksg/ghost/internal/util/swagger" // Swagger stuffs
	"bitbucket.org/buuuksg/ghost/pkg/server"
	"bitbucket.org/buuuksg/ghost/pkg/server/middleware/jwt"
	"bitbucket.org/buuuksg/ghost/pkg/util/crypter"
)

func main() {
	cfg, err := config.Load()
	checkErr(err)

	db, err := dbutil.New(cfg.DbPsn, cfg.DbLog)
	checkErr(err)
	defer db.Close()

	// Initialize HTTP server
	e := server.New(&server.Config{
		Stage:        cfg.Stage,
		Port:         cfg.Port,
		ReadTimeout:  cfg.ReadTimeout,
		WriteTimeout: cfg.WriteTimeout,
		AllowOrigins: cfg.AllowOrigins,
		Debug:        cfg.Debug,
	})

	// Static page for Swagger API specs
	e.Static("/swaggerui", "swaggerui")

	// Initialize DB interfaces
	userDB := user.NewDB()
	userRelationshipDB := userrelationship.NewDB()
	feedDB := feed.NewDB()
	feedTaggedDB := usertaggedfeed.NewDB()
	feedHiddenDB := userhiddenfeed.NewDB()
	likeDB := like.NewDB()
	commentDB := comment.NewDB()
	shareDB := share.NewDB()

	// Initialize services
	crypterSvc := crypter.New()
	rbacSvc := rbac.New(cfg.Debug)
	jwtSvc := jwt.New(cfg.JwtSigningAlgorithm, cfg.JwtSecret, cfg.JwtDuration)
	authSvc := auth.New(db, userDB, jwtSvc, crypterSvc)
	userSvc := user.New(db, userDB, userRelationshipDB, rbacSvc, crypterSvc)
	feedSvc := feed.New(db, feedDB, userSvc, feedTaggedDB, feedHiddenDB, likeDB, shareDB)
	commentSvc := comment.New(db, commentDB, feedDB)

	// Initialize root API
	auth.NewHTTP(authSvc, e)

	// Initialize v1 API
	v1Router := e.Group("/v1")
	v1Router.Use(jwtSvc.MWFunc())

	user.NewHTTP(userSvc, authSvc, v1Router.Group("/users"))
	feed.NewHTTP(feedSvc, authSvc, v1Router.Group("/feeds"))
	comment.NewHTTP(commentSvc, authSvc, v1Router.Group("/comments"))

	// Start the HTTP server
	server.Start(e, cfg.Stage == "development")
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
