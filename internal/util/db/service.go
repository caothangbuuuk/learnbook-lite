package dbutil

import (
	ghostdbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/imdatngo/gowhere"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // DB adapter
)

// New creates new database connection to the database server
func New(dbPsn string, enableLog bool) (*gorm.DB, error) {
	gowhere.DefaultConfig.Dialect = gowhere.DialectMySQL
	return ghostdbutil.New("mysql", dbPsn, enableLog)
}

// NewDB creates new DB instance
func NewDB(model interface{}) *ghostdbutil.DB {
	return &ghostdbutil.DB{Model: model}
}
