package swagger

import (
	_ "bitbucket.org/buuuksg/ghost/pkg/util/swagger" // Swagger stuffs
)

// ListRequest holds data of listing request from react-admin
// swagger:parameters usersList feedsList
type ListRequest struct {
	// Number of records per page
	// default: 25
	Limit int `json:"l,omitempty" query:"l"`
	// Current page number
	// default: 1
	Page int `json:"p,omitempty" query:"p"`
	// Field name for sorting
	// default:
	Sort string `json:"s,omitempty" query:"s"`
	// Sort direction, must be one of ASC, DESC
	// default:
	Order string `json:"o,omitempty" query:"o"`
	// JSON string of filter. E.g: {"field_name":"value"}
	// default:
	Filter string `json:"f,omitempty" query:"f"`
}
