package userhiddenfeed

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
)

// NewDB returns a new user_hidden_feeds database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.UserHiddenFeed{})}
}

// DB represents the client for user_hidden_feeds table
type DB struct {
	*dbutil.DB
}
