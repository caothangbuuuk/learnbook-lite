package like

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
)

// NewDB returns a new Like database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.Like{})}
}

// DB represents the client for likes table
type DB struct {
	*dbutil.DB
}
