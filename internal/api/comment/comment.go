package comment

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	"bitbucket.org/buuuksg/ghost/pkg/server"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
)

// List lists all comments from a feed
func (s *Comment) List(feedId int, lq *dbutil.ListQueryCondition, count *int) ([]*model.Comment, error) {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, feedId); err != nil {
		return nil, err
	}

	var comments []*model.Comment
	if err := s.cdb.ListComments(s.db, &comments, lq, count); err != nil {
		return nil, err
	}

	return comments, nil
}

// Create creates a new comment on the feed by current user
func (s *Comment) Create(authUsr *model.AuthUser, data CreateData) error {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, data.FeedID); err != nil {
		return err
	}

	comment := model.Comment{
		FeedID:   data.FeedID,
		UserID:   authUsr.ID,
		Content:  data.Content,
		Depth:    0,
		ParentID: 0, // root comment will have parentId = 0
	}
	if err := s.cdb.Create(s.db, &comment); err != nil {
		return server.NewHTTPInternalError("Error commenting on feed").SetInternal(err)
	}

	// TODO: should run and return error in the background because user doesn't need to know this
	// Increase comment count
	var commentList []*model.Comment
	count := 0
	db := s.db.Where("feed_id = ?", data.FeedID)
	if err := s.cdb.List(db, &commentList, nil, &count); err != nil {
		return server.NewHTTPInternalError("Error counting comments on feed").SetInternal(err)
	}

	if err := s.fdb.Update(s.db, map[string]interface{}{"comment_count": count}, data.FeedID); err != nil {
		return server.NewHTTPInternalError("Error saving new comment count on feed").SetInternal(err)
	}

	return nil
}

// Update updates a comment by current user
func (s *Comment) Update(authUsr *model.AuthUser, commentId int, data UpdateData) error {
	var comment model.Comment
	if err := s.cdb.ViewComment(s.db, &comment, commentId); err != nil {
		return err
	}

	if comment.UserID != authUsr.ID {
		return server.NewHTTPInternalError("Only comment's owner can modify the comment")
	}

	comment.Content = data.Content
	if err := s.cdb.Update(s.db, comment, comment.ID); err != nil {
		return err
	}

	return nil
}

// Reply replies to a previous comment
func (s *Comment) Reply(authUsr *model.AuthUser, commentId int, data ReplyData) error {
	var comment model.Comment
	if err := s.cdb.ViewComment(s.db, &comment, commentId); err != nil {
		return err
	}

	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, comment.FeedID); err != nil {
		return err
	}

	// if the comment that we are replying to already has parent_id then that parent_id also becomes the current comment's parent_id
	// so that we can ensure one level of depth (by controlling parent_id)
	var parentID int
	if parentID = comment.ParentID; comment.ParentID == 0 {
		parentID = comment.ID
	}
	reply := model.Comment{
		FeedID:   feed.ID,
		UserID:   authUsr.ID,
		Content:  data.Content,
		ParentID: parentID,
		Depth:    1, // demonstrate one level depth comment reply
	}
	if err := s.cdb.Create(s.db, &reply); err != nil {
		return server.NewHTTPInternalError("Error replying on previous comment").SetInternal(err)
	}

	// TODO: should run and return error in the background because user doesn't need to know this
	// Increase comment count
	var commentList []*model.Comment
	count := 0
	db := s.db.Where("feed_id = ?", feed.ID)
	if err := s.cdb.List(db, &commentList, nil, &count); err != nil {
		return server.NewHTTPInternalError("Error counting comments on feed").SetInternal(err)
	}

	if err := s.fdb.Update(s.db, map[string]interface{}{"comment_count": count}, feed.ID); err != nil {
		return server.NewHTTPInternalError("Error saving new comment count on feed").SetInternal(err)
	}

	return nil
}

// Delete deletes a comment
func (s *Comment) Delete(authUsr *model.AuthUser, commentId int) error {
	var comment model.Comment
	if err := s.cdb.ViewComment(s.db, &comment, commentId); err != nil {
		return err
	}

	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, comment.FeedID); err != nil {
		return err
	}

	// Update comment status if this comment belongs to the current user OR the feed's owner is current user
	if comment.UserID == authUsr.ID || feed.UserID == authUsr.ID {
		if err := s.cdb.Update(s.db, map[string]interface{}{"is_deleted": true}, comment.ID); err != nil {
			return err
		}

		return nil
	}

	return server.NewHTTPInternalError("Only feed creator can delete other people's comments")
}
