package comment

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	"bitbucket.org/buuuksg/ghost/pkg/server"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/jinzhu/gorm"
)

// NewDB returns a new Comment database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.Comment{})}
}

// DB represents the client for comments table
type DB struct {
	*dbutil.DB
}

// ListComments lists all comments
func (d *DB) ListComments(db *gorm.DB, comments interface{}, lq *dbutil.ListQueryCondition, count *int) error {
	db = db.Scopes(ActiveComment)

	if err := d.List(db, comments, lq, count); err != nil {
		return server.NewHTTPInternalError("Error getting comments from feed").SetInternal(err)
	}

	return nil
}

// ViewComment views a single comment
func (d *DB) ViewComment(db *gorm.DB, comment *model.Comment, commentId int) error {
	db = db.Scopes(ActiveComment)

	if err := d.View(db, &comment, map[string]interface{}{"id": commentId}); err != nil {
		return server.NewHTTPInternalError("Comment not found").SetInternal(err)
	}

	return nil
}

// ActiveComment limits to active comment only
func ActiveComment(db *gorm.DB) *gorm.DB {
	return db.Where("is_deleted = ?", false)
}
