package comment

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/jinzhu/gorm"
)

// New creates new comment application service
func New(db *gorm.DB, cdb CommentDB, fdb FeedDB) *Comment {
	return &Comment{
		db:  db,
		cdb: cdb,
		fdb: fdb,
	}
}

// CommentDB represents Comment repository interface
type CommentDB interface {
	dbutil.Intf
	ListComments(db *gorm.DB, comments interface{}, lq *dbutil.ListQueryCondition, count *int) error
	ViewComment(db *gorm.DB, comment *model.Comment, commentId int) error
}

// FeedDB represents Feed repository interface
type FeedDB interface {
	dbutil.Intf
	ViewFeed(db *gorm.DB, feed *model.Feed, feedId int) error
}

// Comment represents comment application service
type Comment struct {
	db  *gorm.DB
	cdb CommentDB
	fdb FeedDB
}
