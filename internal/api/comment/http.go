package comment

import (
	"net/http"

	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"

	httputil "bitbucket.org/buuuksg/ghost/pkg/util/http"

	"bitbucket.org/buuuksg/ghost/internal/model"
	"github.com/labstack/echo/v4"
)

// HTTP represents comment http service
type HTTP struct {
	svc  Service
	auth model.Auth
}

// Service represents feed application interface
type Service interface {
	List(feedId int, lq *dbutil.ListQueryCondition, count *int) ([]*model.Comment, error)
	Create(authUsr *model.AuthUser, data CreateData) error
	Update(authUsr *model.AuthUser, commentId int, data UpdateData) error
	Reply(authUsr *model.AuthUser, commentId int, data ReplyData) error
	Delete(authUsr *model.AuthUser, commentId int) error
}

// CreateData contains create comment data from json request
// swagger:model CreateData
type CreateData struct {
	// example: This is my comment
	Content string `json:"content" validate:"required"`
	// example: 1
	FeedID int `json:"feed_id" validate:"required"`
}

// UpdateData contains update comment data from json request
// swagger:model UpdateData
type UpdateData struct {
	// example: This is my updated comment
	Content string `json:"content" validate:"required"`
}

// ReplyData contains reply comment data from json request
// swagger:model ReplyData
type ReplyData struct {
	// example: This is my content
	Content string `json:"content" validate:"required"`
}

// ListResp contains list of paginated comments and total numbers of comments
// swagger:model CommentListResp
type ListResp struct {
	// example: [{"id": 1, "created_at": "2020-01-14T10:03:41Z", "updated_at": "2020-01-14T10:03:41Z", "UserID": "1", "FeedID": "3", "Depth": "3", "ParentID": "2", "Content": "This is a comment", "IsDeleted": "false"]}]
	Data []*model.Comment `json:"data"`
	// example: 1
	TotalCount int `json:"total_count"`
}

// NewHTTP creates new comment http service
func NewHTTP(svc Service, auth model.Auth, eg *echo.Group) {
	h := HTTP{svc, auth}

	// swagger:operation GET /v1/comments/feeds/{id} comments commentsList
	// ---
	// summary: Returns list of comments
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     description: List of comments
	//     schema:
	//       "$ref": "#/definitions/CommentListResp"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.GET("/feeds/:id", h.list)

	// swagger:operation POST /v1/comments comments commentsCreate
	// ---
	// summary: Creates a new comment
	// parameters:
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/CreateData"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.POST("", h.create)

	// swagger:operation PUT /v1/comments/{id} comments commentsUpdate
	// ---
	// summary: Updates a comment
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/UpdateData"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.PUT("/:id", h.update)

	// swagger:operation POST /v1/comments/{id}/reply comments commentsReply
	// ---
	// summary: Replies to a previous comment
	// parameters:
	// - name: id
	//   in: path
	//   description: id of comment
	//   type: integer
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/ReplyData"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.POST("/:id/reply", h.reply)

	// swagger:operation DELETE /v1/comments/{id} comments commentsDelete
	// ---
	// summary: Deletes a comment
	// parameters:
	// - name: id
	//   in: path
	//   description: id of comment
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.DELETE("/:id", h.delete)
}

func (h *HTTP) list(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	lq, err := httputil.ReqListQuery(c)
	if err != nil {
		return err
	}
	count := 0

	resp, err := h.svc.List(id, lq, &count)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, ListResp{resp, count})
}

func (h *HTTP) create(c echo.Context) error {
	r := CreateData{}
	if err := c.Bind(&r); err != nil {
		return err
	}
	if err := h.svc.Create(h.auth.User(c), r); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) update(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	r := UpdateData{}
	if err := c.Bind(&r); err != nil {
		return err
	}
	if err := h.svc.Update(h.auth.User(c), id, r); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) reply(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	r := ReplyData{}
	if err := c.Bind(&r); err != nil {
		return err
	}
	if err := h.svc.Reply(h.auth.User(c), id, r); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	if err := h.svc.Delete(h.auth.User(c), id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
