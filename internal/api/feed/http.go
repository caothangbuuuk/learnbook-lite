package feed

import (
	"net/http"

	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	httputil "bitbucket.org/buuuksg/ghost/pkg/util/http"
	"github.com/labstack/echo/v4"
)

// HTTP represents feed http service
type HTTP struct {
	svc  Service
	auth model.Auth
}

// Service represents feed application interface
type Service interface {
	List(authUsr *model.AuthUser, lq *dbutil.ListQueryCondition, count *int) ([]*model.Feed, error)
	Create(authUsr *model.AuthUser, data CreateData) error
	Update(authUsr *model.AuthUser, feedId int, data UpdateData) error
	Delete(authUsr *model.AuthUser, feedId int) error
	Hide(authUsr *model.AuthUser, feedId int) error
	Like(authUsr *model.AuthUser, feedId int) error
	Share(authUsr *model.AuthUser, feedId int, data ShareData) error
}

// ListResp contains list of paginated feeds and total numbers of feeds
// swagger:model FeedListResp
type ListResp struct {
	// example: [{"id": 1, "created_at": "2020-01-14T10:03:41Z", "updated_at": "2020-01-14T10:03:41Z", "UserID": "1", "Content": "SG", "IsDeleted": "false", "IsHidden": "false", "LikeCount": 3, "ShareCount": 5, "CommentCount": 4}]
	Data []*model.Feed `json:"data"`
	// example: 1
	TotalCount int `json:"total_count"`
}

// ShareData contains share data from json request
// swagger:model ShareData
type ShareData struct {
	// example: This is the feed that I personally recommend you should have a look at
	Content string `json:"content" validate:"required"`
}

// CreateData contains create data from json request
// swagger:model CreateData
type CreateData struct {
	// example: This is my new feed
	Content string `json:"content" validate:"required"`
}

// UpdateData contains update data from json request
// swagger:model UpdateData
type UpdateData struct {
	// example: This is my new updated feed
	Content string `json:"content" validate:"required"`
}

// NewHTTP creates new feed http service
func NewHTTP(svc Service, auth model.Auth, eg *echo.Group) {
	h := HTTP{svc, auth}

	// swagger:operation GET /v1/feeds feeds feedsList
	// ---
	// summary: Returns list of feeds
	// responses:
	//   "200":
	//     description: List of feeds
	//     schema:
	//       "$ref": "#/definitions/FeedListResp"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.GET("", h.list)

	// swagger:operation POST /v1/feeds feeds feedsCreate
	// ---
	// summary: Creates a new feed
	// parameters:
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/CreateData"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.POST("", h.create)

	// swagger:operation PUT /v1/feeds/{id} feeds feedsUpdate
	// ---
	// summary: Updates a feed
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/UpdateData"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.PUT("/:id", h.update)

	// swagger:operation POST /v1/feeds/{id}/hide feeds feedsHide
	// ---
	// summary: Hides a feed
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.POST("/:id/hide", h.hide)

	// swagger:operation DELETE /v1/feeds/{id} feeds feedsDelete
	// ---
	// summary: Deletes a feed
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.DELETE("/:id", h.delete)

	// swagger:operation POST /v1/feeds/{id}/like feeds feedsLike
	// ---
	// summary: Likes a feed
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.POST("/:id/like", h.like)

	// swagger:operation POST /v1/feeds/{id}/share feeds feedsShare
	// ---
	// summary: Shares a feed
	// parameters:
	// - name: id
	//   in: path
	//   description: id of feed
	//   type: integer
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/ShareData"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/errDetails"
	//   "401":
	//     "$ref": "#/responses/errDetails"
	//   "403":
	//     "$ref": "#/responses/errDetails"
	//   "404":
	//     "$ref": "#/responses/errDetails"
	//   "500":
	//     "$ref": "#/responses/errDetails"
	eg.POST("/:id/share", h.share)
}

func (h *HTTP) list(c echo.Context) error {
	lq, err := httputil.ReqListQuery(c)
	if err != nil {
		return err
	}

	count := 0
	resp, err := h.svc.List(h.auth.User(c), lq, &count)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, ListResp{resp, count})
}

func (h *HTTP) create(c echo.Context) error {
	r := CreateData{}
	if err := c.Bind(&r); err != nil {
		return err
	}

	if err := h.svc.Create(h.auth.User(c), r); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) update(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	r := UpdateData{}
	if err := c.Bind(&r); err != nil {
		return err
	}

	if err := h.svc.Update(h.auth.User(c), id, r); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) hide(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	if err := h.svc.Hide(h.auth.User(c), id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	if err := h.svc.Delete(h.auth.User(c), id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) like(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	if err := h.svc.Like(h.auth.User(c), id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) share(c echo.Context) error {
	id, err := httputil.ReqID(c)
	if err != nil {
		return err
	}

	r := ShareData{}
	if err := c.Bind(&r); err != nil {
		return err
	}

	if err := h.svc.Share(h.auth.User(c), id, r); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
