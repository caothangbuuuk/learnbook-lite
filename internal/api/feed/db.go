package feed

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	"bitbucket.org/buuuksg/ghost/pkg/server"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/jinzhu/gorm"
)

// DB represents the client for feed table
type DB struct {
	*dbutil.DB
}

// NewDB returns a new feed database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.Feed{})}
}

// ListFeeds lists all feeds
func (d *DB) ListFeeds(db *gorm.DB, lq *dbutil.ListQueryCondition, count *int) ([]*model.Feed, error) {
	// add is_liked flag while joining to determine which feed was liked by current user
	db = db.Select(
		"DISTINCT feeds.id, " +
			"feeds.created_at, " +
			"feeds.updated_at, " +
			"feeds.user_id, " +
			"feeds.content, " +
			"feeds.is_deleted, " +
			"feeds.is_hidden, " +
			"feeds.like_count, " +
			"feeds.share_count, " +
			"feeds.comment_count, " +
			"CASE WHEN l.feed_id IS NOT NULL THEN 'True' ELSE 'False' END AS is_liked").Joins("left join likes l on feeds.id = l.feed_id").Order("feeds.created_at DESC").Scopes(ActiveFeed)

	var feeds []*model.Feed
	if err := d.List(db, &feeds, lq, count); err != nil {
		return nil, err
	}

	// recount feeds manually since the List count doesn't count distinct query correctly
	*count = len(feeds)

	return feeds, nil
}

// ViewFeed views a single feed
func (d *DB) ViewFeed(db *gorm.DB, feed *model.Feed, feedId int) error {
	db = db.Scopes(ActiveFeed)

	if err := d.View(db, &feed, map[string]interface{}{"id": feedId}); err != nil {
		return server.NewHTTPInternalError("Feed not found").SetInternal(err)
	}

	return nil
}

// ActiveFeed limits to active feed only
func ActiveFeed(db *gorm.DB) *gorm.DB {
	return db.Where("is_deleted = ? AND is_hidden = ?", false, false)
}
