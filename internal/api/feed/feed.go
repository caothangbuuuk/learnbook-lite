package feed

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	"bitbucket.org/buuuksg/ghost/pkg/server"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
)

// List returns list of feeds
func (s *Feed) List(authUsr *model.AuthUser, lq *dbutil.ListQueryCondition, count *int) ([]*model.Feed, error) {
	// Get all current user's friend ids in order to get their feeds
	friendIds, err := s.userSvc.FindFriends(authUsr.ID)
	if err != nil {
		return nil, server.NewHTTPInternalError("Error getting friend's feeds").SetInternal(err)
	}

	// Get all feed ids that current user's friends were tagged in
	feedsTaggedIds, err := s.ftdb.GetByUserIDs(s.db, friendIds)
	if err != nil {
		return nil, server.NewHTTPInternalError("Error getting friend's tagged feeds").SetInternal(err)
	}

	// Get feeds that are:
	// - Current user's feeds
	// - Current user's friend's feeds
	// - Feeds that current user's friend's were tagged in
	db := s.db.Where("(feeds.user_id = ? OR feeds.user_id IN (?) OR feeds.id IN (?))", authUsr.ID, friendIds, feedsTaggedIds)
	feeds, err := s.fdb.ListFeeds(db, lq, count)
	if err != nil {
		return nil, server.NewHTTPInternalError("Error getting feeds").SetInternal(err)
	}

	return feeds, nil
}

// Create creates a new feed by current user
func (s *Feed) Create(authUsr *model.AuthUser, data CreateData) error {
	feed := model.Feed{
		Content: data.Content,
		UserID:  authUsr.ID,
	}

	if err := s.fdb.Create(s.db, &feed); err != nil {
		return server.NewHTTPInternalError("Error creating new feed").SetInternal(err)
	}

	return nil
}

// Hide hides a feed from current user's new feed
func (s *Feed) Hide(authUsr *model.AuthUser, feedId int) error {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, feedId); err != nil {
		return err
	}

	// If this feed belong to the current user then change its status
	if feed.UserID == authUsr.ID {
		if err := s.fdb.Update(s.db, map[string]interface{}{"is_hidden": true}, feedId); err != nil {
			return server.NewHTTPInternalError("Error hiding your feed").SetInternal(err)
		}
		return nil
	}

	// If this feed belongs to another user then insert a record into user_hidden_feeds table in order to not show in new feeds
	hiddenFeed := model.UserHiddenFeed{
		UserID: authUsr.ID,
		FeedID: feedId,
	}

	if existed, err := s.fhdb.Exist(s.db, map[string]interface{}{"feed_id": feedId, "user_id": authUsr.ID}); existed {
		return server.NewHTTPInternalError("Feed was already hidden").SetInternal(err)
	}

	if err := s.fhdb.Create(s.db, &hiddenFeed); err != nil {
		return server.NewHTTPInternalError("Error hiding feed").SetInternal(err)
	}

	return nil
}

// Update updates a feed
func (s *Feed) Update(authUsr *model.AuthUser, feedId int, data UpdateData) error {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, feedId); err != nil {
		return err
	}

	if feed.UserID != authUsr.ID {
		return server.NewHTTPInternalError("Only feed creator can modify the feed")
	}

	updateFeed := model.Feed{
		Content: data.Content,
	}
	if err := s.fdb.Update(s.db, updateFeed, feedId); err != nil {
		return server.NewHTTPInternalError("Error updating feed").SetInternal(err)
	}

	return nil
}

// Like likes a feed by current user
func (s *Feed) Like(authUsr *model.AuthUser, feedId int) error {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, feedId); err != nil {
		return err
	}

	if existed, err := s.ldb.Exist(s.db, map[string]interface{}{"feed_id": feedId, "user_id": authUsr.ID}); existed {
		return server.NewHTTPInternalError("You've already liked this feed").SetInternal(err)
	}

	likeFeed := model.Like{
		UserID: authUsr.ID,
		FeedID: feedId,
	}
	if err := s.ldb.Create(s.db, &likeFeed); err != nil {
		return server.NewHTTPInternalError("Error liking feed").SetInternal(err)
	}

	// TODO: should run and return error in the background because user doesn't need to know this
	// Increase like count
	var likeList []*model.Like
	count := 0
	db := s.db.Where("feed_id = ?", feedId)
	if err := s.ldb.List(db, &likeList, nil, &count); err != nil {
		return server.NewHTTPInternalError("Error counting likes on feed").SetInternal(err)
	}

	if err := s.fdb.Update(s.db, map[string]interface{}{"like_count": count}, feedId); err != nil {
		return server.NewHTTPInternalError("Error saving new like count on feed").SetInternal(err)
	}

	return nil
}

// Delete deletes a feed created by current user
func (s *Feed) Delete(authUsr *model.AuthUser, feedId int) error {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, feedId); err != nil {
		return err
	}

	if feed.UserID != authUsr.ID {
		return server.NewHTTPInternalError("Only feed creator can delete feed")
	}

	if err := s.fdb.Update(s.db, map[string]interface{}{"is_deleted": true}, feedId); err != nil {
		return server.NewHTTPInternalError("Error deleting your feed").SetInternal(err)
	}

	return nil
}

// Share shares a feed by current user
func (s *Feed) Share(authUsr *model.AuthUser, feedId int, data ShareData) error {
	var feed model.Feed
	if err := s.fdb.ViewFeed(s.db, &feed, feedId); err != nil {
		return err
	}

	shareFeed := model.Share{
		UserID: authUsr.ID,
		FeedID: feedId,
	}
	if err := s.sdb.Create(s.db, &shareFeed); err != nil {
		return server.NewHTTPInternalError("Error sharing feed").SetInternal(err)
	}

	// Create a new feed
	newFeed := model.Feed{
		UserID:  authUsr.ID,
		Content: data.Content,
	}
	if err := s.fdb.Create(s.db, &newFeed); err != nil {
		return server.NewHTTPInternalError("Error creating new feed").SetInternal(err)
	}

	// TODO: should run and return error in the background because user doesn't need to know this
	// Increase share count
	var shareList []*model.Share
	count := 0
	db := s.db.Where("feed_id = ?", feedId)
	if err := s.sdb.List(db, &shareList, nil, &count); err != nil {
		return server.NewHTTPInternalError("Error counting shares on feed").SetInternal(err)
	}

	if err := s.fdb.Update(s.db, map[string]interface{}{"share_count": count}, feedId); err != nil {
		return server.NewHTTPInternalError("Error saving new share count on feed").SetInternal(err)
	}

	return nil
}
