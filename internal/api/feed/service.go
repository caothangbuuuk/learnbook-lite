package feed

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/jinzhu/gorm"
)

// New creates new feed application service
func New(db *gorm.DB, fdb FeedDB, userSvc UserService, ftdb FeedTaggedDB, fhdb FeedHiddenDB, ldb LikeDB, sdb ShareDB) *Feed {
	return &Feed{
		db:      db,
		fdb:     fdb,
		userSvc: userSvc,
		ftdb:    ftdb,
		fhdb:    fhdb,
		ldb:     ldb,
		sdb:     sdb,
	}
}

// UserService represents user service interface
type UserService interface {
	FindFriends(uid int) ([]int, error)
}

// FeedTaggedDB represents user tagged feeds repository interface
type FeedTaggedDB interface {
	GetByUserIDs(db *gorm.DB, uids []int) ([]int, error)
}

// FeedHiddenDB represents user hidden feeds repository interface
type FeedHiddenDB interface {
	dbutil.Intf
}

// LikeDB represents Like repository interface
type LikeDB interface {
	dbutil.Intf
}

// ShareDB represents Share repository interface
type ShareDB interface {
	dbutil.Intf
}

// FeedDB represents feed repository interface
type FeedDB interface {
	dbutil.Intf
	ListFeeds(db *gorm.DB, lq *dbutil.ListQueryCondition, count *int) ([]*model.Feed, error)
	ViewFeed(db *gorm.DB, feed *model.Feed, feedId int) error
}

// Feed represents feed application service
type Feed struct {
	db      *gorm.DB
	fdb     FeedDB
	userSvc UserService
	ftdb    FeedTaggedDB
	fhdb    FeedHiddenDB
	ldb     LikeDB
	sdb     ShareDB
}
