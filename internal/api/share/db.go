package share

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
)

// NewDB returns a new Share database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.Share{})}
}

// DB represents the client for shares table
type DB struct {
	*dbutil.DB
}
