package usertaggedfeed

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/imdatngo/gowhere"
	"github.com/jinzhu/gorm"
)

// NewDB returns a new user_tagged_feeds database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.UserTaggedFeed{})}
}

// DB represents the client for user_tagged_feeds table
type DB struct {
	*dbutil.DB
}

// GetByUserIDs returns list of feed ids from list of user ids
func (d *DB) GetByUserIDs(db *gorm.DB, uids []int) ([]int, error) {
	lq := &dbutil.ListQueryCondition{
		Filter: gowhere.Where("user_id IN (?)", uids),
	}

	var utf []*model.UserTaggedFeed
	if err := d.List(db, &utf, lq, nil); err != nil {
		return nil, err
	}

	var taggedFeeds []int
	for _, v := range utf {
		taggedFeeds = append(taggedFeeds, v.FeedID)
	}

	return taggedFeeds, nil
}
