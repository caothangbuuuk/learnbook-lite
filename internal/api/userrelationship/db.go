package userrelationship

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
)

// NewDB returns a new user relationship database instance
func NewDB() *DB {
	return &DB{dbutil.NewDB(model.UserRelationship{})}
}

// DB represents the client for user relationship table
type DB struct {
	*dbutil.DB
}
