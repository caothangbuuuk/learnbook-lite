package user

import (
	"bitbucket.org/buuuksg/ghost/internal/model"
	"bitbucket.org/buuuksg/ghost/pkg/rbac"
	dbutil "bitbucket.org/buuuksg/ghost/pkg/util/db"
	"github.com/jinzhu/gorm"
)

// New creates new user application service
func New(db *gorm.DB, udb MyDB, urdb URDB, rbacSvc rbac.Intf, cr Crypter) *User {
	return &User{db: db, udb: udb, urdb: urdb, rbac: rbacSvc, cr: cr}
}

// User represents user application service
type User struct {
	db   *gorm.DB
	udb  MyDB
	urdb URDB
	rbac rbac.Intf
	cr   Crypter
}

// URDB represents user relationship repository interface
type URDB interface {
	dbutil.Intf
}

// MyDB represents user repository interface
type MyDB interface {
	dbutil.Intf
	FindByUsername(db *gorm.DB, username string) (*model.User, error)
}

// Crypter represents security interface
type Crypter interface {
	CompareHashAndPassword(hasedPwd string, rawPwd string) bool
	HashPassword(string) string
}
