package migration

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/buuuksg/ghost/config"
	"bitbucket.org/buuuksg/ghost/internal/model"
	dbutil "bitbucket.org/buuuksg/ghost/internal/util/db"
	"bitbucket.org/buuuksg/ghost/pkg/util/crypter"
	"bitbucket.org/buuuksg/ghost/pkg/util/migration"
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var defaultTableOpts = "ENGINE=InnoDB ROW_FORMAT=DYNAMIC"

// Base represents base columns for all tables. Do not use gorm.Model because of uint ID
type Base struct {
	ID        int `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

// Run executes the migration
func Run() (respErr error) {
	cfg, err := config.Load()
	if err != nil {
		return err
	}

	db, err := dbutil.New(cfg.DbPsn, false)
	if err != nil {
		return err
	}
	defer db.Close()

	defer func() {
		if r := recover(); r != nil {
			switch x := r.(type) {
			case string:
				respErr = fmt.Errorf("%s", x)
			case error:
				respErr = x
			default:
				respErr = fmt.Errorf("Unknown error: %+v", x)
			}
		}
	}()

	// workaround for "Index column size too large" error on migrations table
	initSQL := "CREATE TABLE IF NOT EXISTS migrations (id VARCHAR(255) PRIMARY KEY) " + defaultTableOpts
	if err := db.Exec(initSQL).Error; err != nil {
		return err
	}

	migration.Run(db, []*gormigrate.Migration{
		// create initial tables
		{
			ID: "201905051012",
			Migrate: func(tx *gorm.DB) error {
				// it's a good practice to copy the struct inside the function,
				// so side effects are prevented if the original struct changes during the time

				type Company struct {
					Base
					Name string `gorm:"type:varchar(255)"`
				}

				type User struct {
					Base
					CompanyID    int `gorm:"index"`
					Company      *Company
					FirstName    string `gorm:"type:varchar(255)"`
					LastName     string `gorm:"type:varchar(255)"`
					Address      string `gorm:"type:varchar(255)"`
					Email        string `gorm:"type:varchar(255)"`
					Mobile       string `gorm:"type:varchar(255)"`
					Username     string `gorm:"type:varchar(255);unique_index;not null"`
					Password     string `gorm:"type:varchar(255);not null"`
					LastLogin    *time.Time
					Blocked      bool   `gorm:"not null;default:0"`
					RefreshToken string `gorm:"type:varchar(255)"`
					Role         string `gorm:"type:varchar(255)"`
				}

				if err := tx.Set("gorm:table_options", defaultTableOpts).AutoMigrate(&Company{}, &User{}).Error; err != nil {
					return err
				}

				dummyUsers := []*User{
					{
						Username:  "superadmin",
						Password:  os.Getenv("SUPERADMIN_PWD"),
						CompanyID: 1,
						Email:     "superadmin@ghost.com",
						Role:      model.RoleSuperAdmin,
					},
					{
						Username:  "admin",
						Password:  os.Getenv("ADMIN_PWD"),
						CompanyID: 1,
						Email:     "admin@ghost.com",
						Role:      model.RoleAdmin,
					},
					{
						Username:  "thang",
						Password:  "123456",
						CompanyID: 1,
						Email:     "thang@ghost.com",
						Role:      model.RoleUser,
					},
					{
						Username:  "thang_friend",
						Password:  "123456",
						CompanyID: 1,
						Email:     "thang_friend@ghost.com",
						Role:      model.RoleUser,
					},
					{
						Username:  "sam",
						Password:  "123456",
						CompanyID: 1,
						Email:     "sam@ghost.com",
						Role:      model.RoleUser,
					},
					{
						Username:  "sam_friend",
						Password:  "123456",
						CompanyID: 1,
						Email:     "sam_friend@ghost.com",
						Role:      model.RoleUser,
					},
				}
				for _, usr := range dummyUsers {
					if usr.Password == "" {
						usr.Password = usr.Username + "123!@#"
					}
					usr.Password = crypter.HashPassword(usr.Password)
					if err := tx.Create(usr).Error; err != nil {
						return err
					}
				}

				dummyCompanies := []*Company{
					{
						Name: "Buuuk",
					},
					{
						Name: "Facebook",
					},
					{
						Name: "Google",
					},
					{
						Name: "Uber",
					},
				}
				for _, rec := range dummyCompanies {
					if err := tx.Create(rec).Error; err != nil {
						return err
					}
				}

				return nil
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("users", "companies").Error
			},
		},
		{
			ID: "202004191012",
			Migrate: func(tx *gorm.DB) error {
				type RelationshipType struct {
					Base
					Name string `gorm:"type:varchar(255)"`
				}
				type UserRelationship struct {
					Base
					UserID             int    `json:"user_id" gorm:"index"`
					RelatingUserID     int    `json:"relating_user_id" gorm:"index"`
					RelationshipTypeID int    `json:"relationship_type_id"`
					Status             string `json:"name" gorm:"type:varchar(255)"`
				}

				if err := tx.Set("gorm:table_options", defaultTableOpts).AutoMigrate(&RelationshipType{}, &UserRelationship{}).Error; err != nil {
					return err
				}

				defaultRelationshipTypes := []*RelationshipType{
					{
						Name: "friend",
					},
				}
				for _, rt := range defaultRelationshipTypes {
					if err := tx.Create(rt).Error; err != nil {
						return err
					}
				}

				dummyUserRelationships := []*UserRelationship{
					{
						UserID:             3, // thang
						RelatingUserID:     4, // thang_friend
						RelationshipTypeID: 1,
						Status:             "Confirmed",
					},
					{
						UserID:             4, // thang_friend
						RelatingUserID:     3, // thang
						RelationshipTypeID: 1,
						Status:             "Confirmed",
					},
					{
						UserID:             5, // sam
						RelatingUserID:     6, // sam_fiend
						RelationshipTypeID: 1,
						Status:             "Confirmed",
					},
					{
						UserID:             6, // sam_fiend
						RelatingUserID:     5, // sam
						RelationshipTypeID: 1,
						Status:             "Confirmed",
					},
					{
						UserID:             3, // thang
						RelatingUserID:     5, // sam
						RelationshipTypeID: 1,
						Status:             "Confirmed",
					},
					{
						UserID:             5, // sam
						RelatingUserID:     3, // thang
						RelationshipTypeID: 1,
						Status:             "Confirmed",
					},
				}
				for _, ur := range dummyUserRelationships {
					if err := tx.Create(ur).Error; err != nil {
						return err
					}
				}

				return nil
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("user_relationships", "relationship_types").Error
			},
		},
		{
			ID: "202004191212",
			Migrate: func(tx *gorm.DB) error {
				type Feed struct {
					Base
					UserID       int    `gorm:"index"`
					Content      string `gorm:"type:text"`
					IsDeleted    bool   `gorm:"not null;default:false"`
					IsHidden     bool   `gorm:"not null;default:false"`
					LikeCount    int
					ShareCount   int
					CommentCount int
				}
				type FeedAsset struct {
					Base
					FeedID   int    `gorm:"index"`
					AssetUrl string `gorm:"type:varchar(255)"`
				}
				type Like struct {
					Base
					FeedID int `gorm:"index"`
					UserID int `gorm:"index"`
				}
				type Share struct {
					Base
					FeedID int `gorm:"index"`
					UserID int `gorm:"index"`
				}
				type Comment struct {
					Base
					FeedID    int    `gorm:"index"`
					UserID    int    `gorm:"index"`
					ParentID  int    `gorm:"index"`
					Content   string `gorm:"type:text"`
					Depth     int    `gorm:"type:smallint;default:0"`
					IsDeleted bool   `gorm:"not null;default:false"`
				}
				type UserHiddenFeed struct {
					Base
					UserID int `gorm:"index"`
					FeedID int `gorm:"index"`
				}
				type UserTaggedFeed struct {
					Base
					UserID int `gorm:"index"`
					FeedID int `gorm:"index"`
				}

				if err := tx.Set("gorm:table_options", defaultTableOpts).AutoMigrate(&Feed{}, &FeedAsset{}, &Like{}, &Share{}, &Comment{}, &UserHiddenFeed{}, &UserTaggedFeed{}).Error; err != nil {
					return err
				}

				dummyFeeds := []*Feed{
					{
						UserID:  3,
						Content: "This is Thang's post number 1",
					},
					{
						UserID:  3,
						Content: "This is Thang's post number 2",
					},
					{
						UserID:  3,
						Content: "This is Thang's post number 3",
					},
					{
						UserID:  4,
						Content: "This is Thang_friend's post number 1",
					},
					{
						UserID:  4,
						Content: "This is Thang_friend's post number 2",
					},
					{
						UserID:  4,
						Content: "This is Thang_friend's post number 3",
					},
					{
						UserID:  4,
						Content: "This is Thang_friend's post number 4",
					},
					{
						UserID:  5,
						Content: "This is Sam's post number 1",
					},
					{
						UserID:  5,
						Content: "This is Sam's post number 2",
					},
					{
						UserID:  5,
						Content: "This is Sam's post number 3",
					},
					{
						UserID:  6,
						Content: "This is Sam_friend's post number 1",
					},
					{
						UserID:  6,
						Content: "This is Sam_friend's post number 2",
					},
					{
						UserID:  6,
						Content: "This is Sam_friend's post number 3",
					},
					{
						UserID:  6,
						Content: "This is Sam_friend's post number 4",
					},
				}
				for _, ur := range dummyFeeds {
					if err := tx.Create(ur).Error; err != nil {
						return err
					}
				}

				return nil
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("feeds", "feed_assets", "likes", "shares", "comments", "user_hidden_feeds", "user_tagged_feeds").Error
			},
		},
	})

	return nil
}
