package model

// Feed represents the feed model
// swagger:model
type Feed struct {
	Base
	UserID       int    `gorm:"index"`
	Content      string `gorm:"type:text"`
	IsDeleted    bool   `gorm:"not null;default:false"`
	IsHidden     bool   `gorm:"not null;default:false"`
	LikeCount    int
	ShareCount   int
	CommentCount int
	IsLiked      bool
}
