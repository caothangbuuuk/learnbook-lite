package model

// Like represents the like model
// swagger:model
type Like struct {
	Base
	UserID int `gorm:"index"`
	FeedID int `gorm:"index"`
}
