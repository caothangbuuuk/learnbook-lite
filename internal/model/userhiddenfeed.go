package model

// UserHiddenFeed represents the user hidden feed model
// swagger:model
type UserHiddenFeed struct {
	Base
	UserID int `gorm:"index"`
	FeedID int `gorm:"index"`
}
