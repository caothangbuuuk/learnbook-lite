package model

// UserRelationship represents the user relationship model
// swagger:model
type UserRelationship struct {
	Base
	UserID             int    `json:"user_id" gorm:"index"`
	RelatingUserID     int    `json:"relating_user_id" gorm:"index"`
	RelationshipTypeID int    `json:"relationship_type_id"`
	Status             string `json:"name" gorm:"type:varchar(255)"`
}
