package model

// Comment represents the like model
// swagger:model
type Comment struct {
	Base
	FeedID    int    `gorm:"index"`
	UserID    int    `gorm:"index"`
	ParentID  int    `gorm:"index"`
	Content   string `gorm:"type:text"`
	Depth     int    `gorm:"type:smallint;default:0"`
	IsDeleted bool   `gorm:"not null;default:false"`
}
