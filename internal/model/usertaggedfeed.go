package model

// UserTaggedFeed represents the user tagged feed model
// swagger:model
type UserTaggedFeed struct {
	Base
	UserID int `gorm:"index"`
	FeedID int `gorm:"index"`
}
