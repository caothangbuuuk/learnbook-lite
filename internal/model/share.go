package model

// Share represents the share model
// swagger:model
type Share struct {
	Base
	UserID int `gorm:"index"`
	FeedID int `gorm:"index"`
}
