#!/usr/bin/env bash

# Ensure the database container is online and usable
# echo "Waiting for database..."
until docker exec -i ghost-api.db mysql -u ghost -pghost123 -D ghost_dev -e "SELECT 1" &> /dev/null
do
  # printf "."
  sleep 1
done
