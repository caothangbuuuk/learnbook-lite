#!/bin/bash

set -e

apt update && apt install sudo vim make gcc git curl rsyslog systemd -y

# create user
sudo useradd ghost -m
cd /home/ghost

# Install GO
curl -O https://dl.google.com/go/go1.11.4.linux-amd64.tar.gz
tar -xzf go1.11.4.linux-amd64.tar.gz
sudo mv go /usr/local

# Setting GO Paths
echo 'export GOPATH=$HOME/go' | sudo tee -a /home/ghost/.profile
echo 'export PATH="$PATH:/usr/local/go/bin:$GOPATH/bin"' | sudo tee -a /home/ghost/.profile
echo 'export GO111MODULE=on' | sudo tee -a /home/ghost/.profile

# Clone & build server from source
git clone https://bitbucket.org/buuuksg/ghost.git ghost
source /home/ghost/.profile
cd ghost
make build
chown -R ghost:ghost .

# Install systemd service
sudo mv ghost.service /lib/systemd/system/.
sudo chmod 755 /lib/systemd/system/ghost.service

# Modify "/etc/rsyslog.conf" and uncomment the lines below
#   module(load="imtcp")
#   input(type="imtcp" port="514")
# Then, create “/etc/rsyslog.d/30-ghost.conf” with the following content:
#   if $programname == 'ghost' or $syslogtag == 'ghost' then /var/log/ghost/ghost.log
#   & stop
# Now restart the rsyslog service and ghost service. View logs by:
#   tail -f /var/log/ghost/ghost.log

# Install syncer
# */5 * * * * cd /home/ghost/ghost-api && ./syncer >> /var/log/ghost/syncer.log 2>&1
